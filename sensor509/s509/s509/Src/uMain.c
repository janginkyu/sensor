/*
 * uMain.c
 *
 *  Created on: 2018. 5. 14.
 *      Author: leplu
 */

#include "uMain.h"
#include "stm32f7xx_hal.h"
#include "adc.h"
#include "usart.h"

#include <string.h>

uint32_t adcValue[2];

float adcf[2];
uint8_t conv = 0;

// 각 채널이 변환될때 실행됨
void HAL_ADC_ConvCpltCallback(ADC_HandleTypeDef* hadc) {
	static uint8_t uindex = 0;
	if(hadc->Instance == hadc1.Instance) {
		adcValue[uindex] = HAL_ADC_GetValue(&hadc);
		uindex++;
		if(uindex >= 2) {
			uindex = 0;
			for(int i = 0; i < 2; i++) {
				HAL_UART_Transmit(&huart3, "!\n", 2, 10000);
				adcf[i] = (float)adcValue[i] * 3.3f / 4096.0f;
			}
			conv = 1;
		}
	}
}

void uInit() {
	char str[100];
	sprintf(str, "Start!\n");
	HAL_UART_Transmit(&huart3, str, strlen(str), 10000);
}

void uLoop() {
	char str[100];
	static uint32_t prevTime;
	uint32_t currTime = HAL_GetTick();
	if(currTime - prevTime >= 100) {
		// adc1 interrupt mode start
		HAL_ADC_Start_IT(&hadc1);
		prevTime = currTime;
	}
	if(conv) {
		conv = 0;
		// %f 변환은 기본적으로 꺼져있기때문에 링커 설정에서 켠다.
		sprintf(str, "%d : %f, %f\n", currTime, adcf[0], adcf[1]);
		HAL_UART_Transmit(&huart3, str, strlen(str), 10000);
	}
}

/*
 * umain.c
 *
 *  Created on: 2018. 5. 14.
 *      Author: user
 */
#include "umain.h"
#include "stm32f7xx_hal.h"
#include "adc.h"
#include "usart.h"
#include "string.h"

uint32_t uindex=0;
//uint32_t index=0; -> uint32_t uintex=0; 으로 변경 index라고 하는 문자열 함수가 이미 존재하기 때문입니다.
uint32_t adcValue[2];
float adcf[2];
uint8_t conv=0;
void HAL_ADC_ConvCpltCallback(ADC_HandleTypeDef* hadc){
	if(hadc->Instance==hadc1.Instance){
		adcValue[uindex]=HAL_ADC_GetValue(hadc);
		//adcValue[uindex]=HAL_ADC_GetValue(&hadc); -> adcValue[uindex]=HAL_ADC_GetValue(hadc);
		//로 변경하였습니다. 이미 포인터이게 때문에 &는 필요 없습니다.
		uindex++;
		if(uindex>=2){
			uindex=0;
			for(int i=0; i<2; i++){
				adcf[i]=adcValue[i]*3.3f/4096.0f;
			}
			conv=1;
		}
	}
}

void uinit(){
	char str[100];
	sprintf(str, "Start!\r\n");
	HAL_UART_Transmit(&huart3, str, strlen(str), 10000);
}

void uloop(){
	static uint32_t prevTime=0;
	uint32_t currTime=HAL_GetTick();
	if(currTime-prevTime>=100){
		HAL_ADC_Start_IT(&hadc1);
		prevTime=currTime;
	}
	if(conv!=0){
		conv=0;
		char str[100];
		sprintf(str, "%d, %d\r\n", adcValue[0], adcValue[1]);
		HAL_UART_Transmit(&huart3, str, strlen(str), 10000);
	}
}

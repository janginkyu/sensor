/*
 * uMain.c
 *
 *  Created on: 2018. 4. 30.
 *      Author: leplu
 */

#include "uMain.h"
#include "main.h"
#include "stm32f7xx_hal.h"
#include "usart.h"

// external interrupt가 100ms 이내로 여러개 들어오면 무시
// 이전 호출 시간
uint32_t prevTime = 0;

// uint16_t : 16비트 정수
// external interrupt 발생시 호출되는 callback function
// GPIO_Pin으로 interrupt를 발생시킨 pin 번호가 들어온다.
void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin) {
	char str[100];
	if(GPIO_Pin == USER_Btn_Pin){
		// HAL_GetTick() : ms 단위로 현재시간을 반환
		uint32_t currTime = HAL_GetTick();
		if(currTime - prevTime > 100) {
			sprintf(str, "pushed pin number %d\n", GPIO_Pin);
			HAL_UART_Transmit(&huart3, str, strlen(str), 10000);
			prevTime = currTime;
		}
	}
}

void uInit() {
	;
}

void uLoop() {
	// Test_GPIO_Port -> Test : 미리 지정한 pin 별명
	HAL_GPIO_WritePin(Test_GPIO_Port, Test_Pin, GPIO_PIN_SET);
	// delay 100ms
	HAL_Delay(100);
	HAL_GPIO_WritePin(Test_GPIO_Port, Test_Pin, GPIO_PIN_RESET);
	HAL_Delay(100);
}

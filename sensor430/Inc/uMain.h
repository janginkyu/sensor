/*
 * uMain.h
 *
 *  Created on: 2018. 4. 30.
 *      Author: leplu
 */

#ifndef UMAIN_H_
#define UMAIN_H_

void uInit();
void uLoop();

#endif /* UMAIN_H_ */

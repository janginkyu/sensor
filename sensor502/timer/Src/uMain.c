/*
 * uMain.c
 *
 *  Created on: 2018. 5. 2.
 *      Author: leplu
 */

#include "uMain.h"
#include "gpio.h"
#include "tim.h"
#include "stm32f7xx_it.h"

void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim) {
	static int i=0;
	if(htim->Instance == htim1.Instance) {

	}
	if(i%100==0) HAL_GPIO_TogglePin(LD3_GPIO_Port, LD3_Pin);
	i++;
}

void init() {
	HAL_TIM_Base_Start_IT(&htim1);
}

void loop() {

}

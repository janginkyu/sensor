/*
 * uMain.h
 *
 *  Created on: 2018. 5. 2.
 *      Author: leplu
 */

#ifndef UMAIN_H_
#define UMAIN_H_

void init();
void loop();

#endif /* UMAIN_H_ */

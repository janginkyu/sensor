/*
 * uMain.c
 *
 *  Created on: 2018. 5. 12.
 *      Author: leplu
 */

#include "gpio.h"
#include "uMain.h"

#include <stdlib.h>
/*
  // Configure GPIO pin Output Level
  HAL_GPIO_WritePin(GPIOF, U_LED1_Pin|U_LED4_Pin|U_LED6_Pin, GPIO_PIN_RESET);

  // Configure GPIO pin Output Level
  HAL_GPIO_WritePin(GPIOE, U_LED2_Pin|U_LED3_Pin|U_LED5_Pin, GPIO_PIN_RESET);

  // Configure GPIO pin Output Level
  HAL_GPIO_WritePin(GPIOB, LD3_Pin|LD2_Pin, GPIO_PIN_RESET);

  // Configure GPIO pin Output Level
  HAL_GPIO_WritePin(GPIOG, USB_PowerSwitchOn_Pin|U_LED8_Pin|U_LED7_Pin, GPIO_PIN_RESET);
*/

stateList* states;

void uLedOn(uint8_t state) {
	static uint32_t gpio_upin[] = {GPIOF, GPIOE, GPIOE, GPIOF, GPIOE, GPIOF, GPIOG, GPIOG};
	static uint32_t upin[] = {U_LED1_Pin, U_LED2_Pin, U_LED3_Pin, U_LED4_Pin, U_LED5_Pin, U_LED6_Pin, U_LED7_Pin, U_LED8_Pin};
	uint8_t i;
	for(i = 0x00; i < 0x08; i++) {
		HAL_GPIO_WritePin(gpio_upin[i], upin[i], (state & (0x01 << i)));
	}
}

void uInit() {
	states = (stateList*)malloc(sizeof(stateList));
	stateList* states0 = states;
	uint8_t i;
	for(i = 0; i < 14; i++) {
		states->duration = 100;
		if(i < 7) {
			states->state = 0x01 << i;
		}
		else {
			states->state = 0x80 >> (i - 7);
		}

		if(i == 13) {
			states->next = states0;
		}
		else {
			states->next = (stateList*)malloc(sizeof(stateList));
		}
		states = states->next;
	}
}

void uLoop() {
	uLedOn(states->state);
	HAL_Delay(states->duration);
	states = states->next;
}

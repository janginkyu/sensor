/*
 * uMain.h
 *
 *  Created on: 2018. 5. 12.
 *      Author: leplu
 */

#ifndef UMAIN_H_
#define UMAIN_H_

#define ULED_STATE 0x66

typedef struct _stateList {
	uint8_t state;
	uint32_t duration;
	struct _stateList* next;
} stateList;

void uLedOn(uint8_t state);
void uLoop();
void uInit();

#endif /* UMAIN_H_ */

/*
 * uMain.c
 *
 *  Created on: 2018. 4. 23.
 *      Author: leplu
 */

#include <strings.h>
#include "uMain.h"
// #include "main.h" // 사실 main.h include할필요 없는듯
#include "gpio.h"
#include "usart.h"

char data2[100];
char data3[100];
char on = 0;

// interrupt 발생시 호출
void HAL_UART_RxCpltCallback(UART_HandleTypeDef* huart) {
	char d;
	// uart2로 입력이 들어오면
	if(huart->Instance == huart2.Instance) {
		d = data2[0];
		HAL_UART_Transmit(&huart3, &d, 1, 10000);
		HAL_UART_Receive_IT(&huart2, data2, 1);
	}
	// uart3
	else if(huart->Instance == huart3.Instance) {
		d = data3[0];
		HAL_UART_Transmit(&huart2, &d, 1, 10000);
		HAL_UART_Receive_IT(&huart3, data3, 1);
	}
 /*
	if(data3[0] == 'a') {
		on = !on;
	}
	// HAL_UART_Transmit(huart, data, 1, 10000);
	HAL_UART_Receive_IT(huart, data3, 1);

	*/
}

void uInit() {
	// 가변길이 문자열 입력 (interrupt)
	HAL_UART_Receive_IT(&huart2, data2, 1);
	HAL_UART_Receive_IT(&huart3, data3, 1);
}

void uLoop() {
	/* 출력 연습
	static int iter = 0;
	char str[100];
	sprintf(str, "Hello %d\n", iter);
	HAL_UART_Transmit(&huart3, str, strlen(str), 1000);
	HAL_Delay(100);
	iter++;
	*/

	/*
	// 입력 연습 (고정 길이 문자열 입력)
	char str[100];
	char message[100];

	HAL_StatusTypeDef res = HAL_UART_Receive(&huart3, message, 1, 1000);

	// 입력이 제대로 들어오지 않을 경우
	if(res != HAL_OK) {
		sprintf(str, "UART Receive Fail\n");
		HAL_UART_Transmit(&huart3, str, strlen(str), 1000);
	}
	else {
		sprintf(str, "Received : %c\n", message[0]);
		HAL_UART_Transmit(&huart3, str, strlen(str), 1000);
	}

	*/

	if(on) {
		HAL_GPIO_WritePin(LD3_GPIO_Port, LD3_Pin, GPIO_PIN_SET);
	}
	else {
		HAL_GPIO_WritePin(LD3_GPIO_Port, LD3_Pin, GPIO_PIN_RESET);
	}

}

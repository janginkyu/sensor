/*
 * attitude.h
 *
 *  Created on: 2018. 6. 1.
 *      Author: leplu
 */

#ifndef ATTITUDE_H_
#define ATTITUDE_H_

#include "u_mpu6050.h"



#define ANGLE_ZERO 0.06185
#define PHIDOT_ZERO 0.039



double update_angley(Gyro* gy, double angley);
double update_velocity(Gyro* gy, double mean_w, double phidot, double v);


#endif /* ATTITUDE_H_ */

/*
 * controller.h
 *
 *  Created on: 2018. 6. 1.
 *      Author: leplu
 */

#ifndef CONTROLLER_H_
#define CONTROLLER_H_

#define G 9.81

#define M 	0.5
#define m	0.040
#define I 	0.002
#define J 	0.000003
#define R 	0.033
#define L 	0.13



#define W 1.

#include "stm32f7xx_hal.h"

typedef struct {
	double x0; // T integral phi
	double x1; // T phi
	double x2; // Q phi
	double P;
	double Q;
	double T;
	double integralomega;
	double differentialomega;
} State;

typedef struct {
	double Id;
	double Pd;
	double Dd;
} Xref;

typedef struct {
	double K1;
	double K2;
	double K3;
} Gain_in;

typedef struct {
	double Ka;
	double Kb;
	double Kc;
	double Kd;
	double Ki;
	double Kdw;
} Gain_out;

void control(double omega_ref, double* mean_w, double* angley, double* phidot, double* time, double* v, uint8_t curr);
void controller_init();
void outer_loop(double omega_ref, double* mean_w, double* angley, double* phidot, double* time, double* v, uint8_t curr);
void inner_loop(double* mean_w, double* angley, double* phidot, double* time, uint8_t curr);
void estimate_state(double* mean_w, double* angley, double* phidot, double* time, uint8_t curr);
void calculate_inner_gain();
void calculate_outer_gain();

#endif /* CONTROLLER_H_ */

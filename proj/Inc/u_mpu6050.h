/*
 * u_mpu6050.h
 *
 *  Created on: 2018. 5. 22.
 *      Author: leplu
 */

#ifndef U_MPU6050_H_
#define U_MPU6050_H_

#include "sd_hal_mpu6050.h"

#define U_DEVICE SD_MPU6050_Device_0
#define U_GYRO_SENS SD_MPU6050_Gyroscope_1000s
#define U_ACCEL_SENS SD_MPU6050_Accelerometer_8G

typedef struct {
	I2C_HandleTypeDef* phi2c;
	SD_MPU6050* pmpu;
	uint32_t t_10us;
	uint32_t tpre_10us;
	float* gyro;
	float* accel;
	float* gyro_pre;
	float* accel_pre;
} Gyro;

Gyro* u_initialize_gyro();
Gyro* u_update_gyro_value(Gyro* pre);


#endif /* U_MPU6050_H_ */

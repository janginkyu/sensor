/*
 * u_time.h
 *
 *  Created on: 2018. 6. 4.
 *      Author: leplu
 */

#ifndef U_TIME_H_
#define U_TIME_H_

#include "stm32f7xx_hal.h"

uint32_t get10us();
void u_time_init();

#endif /* U_TIME_H_ */

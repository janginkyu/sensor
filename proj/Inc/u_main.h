/*
 * u_main.h
 *
 *  Created on: 2018. 6. 6.
 *      Author: leplu
 */

#ifndef U_MAIN_H_
#define U_MAIN_H_

#define DEBUG

#include "stm32f7xx_hal.h"

#define VAR_QUEUE_SIZE 30
#define START_ANGLE (45. * 3.14159265358979 / 180.)


void uinit();
void uloop();


void set_rot(int8_t in);

void set_speed(int8_t in);

#endif /* U_MAIN_H_ */

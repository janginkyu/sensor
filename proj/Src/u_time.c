/*
 * u_time.c
 *
 *  Created on: 2018. 6. 4.
 *      Author: leplu
 */

#include "u_time.h"
#include "tim.h"
#include "main.h"

uint32_t u_tick = 0;

void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef* htim) {
	if(htim->Instance == htim1.Instance) {
		u_tick++;
	}
}

void u_time_init() {
	HAL_TIM_Base_Start_IT(&htim1);
}

uint32_t get10us() {
	return u_tick;
}

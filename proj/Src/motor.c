/*
 * motor.c
 *
 *  Created on: 2018. 6. 1.
 *      Author: leplu
 */

#include "main.h"
#include "usart.h"
#include "stm32f7xx_hal.h"

#include "motor.h"

#define MAXPWM 250

#define MIN(a, b) (((a)<(b))?(a):(b))

UART_HandleTypeDef* phuart_arduino;

void motor_init(UART_HandleTypeDef* phuart) {
	phuart_arduino = phuart;
	return;
}

void motor_run(int16_t left, int16_t right) {
	char signal[6];

	if(left > 0) {
		signal[0] = MIN(MAXPWM, (uint8_t)(left + MOTOR_BIAS_LEFT));
		signal[1] = 0;
	}
	else if(left < 0) {
		signal[0] = 0;
		signal[1] = MIN(MAXPWM, (uint8_t)(-left + MOTOR_BIAS_LEFT));
	}
	else {
		signal[0] = 0;
		signal[1] = 0;
	}

	if(right > 0) {
		signal[2] = MIN(MAXPWM, (uint8_t)(right + MOTOR_BIAS_RIGHT));
		signal[3] = 0;
	}
	else if(right < 0) {
		signal[2] = 0;
		signal[3] = MIN(MAXPWM, (uint8_t)(-right + MOTOR_BIAS_RIGHT));
	}
	else {
		signal[2] = 0;
		signal[3] = 0;
	}
	HAL_UART_Transmit(phuart_arduino, signal, 4, 100);

	return;
}

int16_t w_pwm_convert(double w, uint8_t motor_index) {
	int16_t pwm;
	switch(motor_index) {
	case LEFT:
		pwm = (int16_t)(w * MOTOR_CONST_LEFT);
		break;
	case RIGHT:
		pwm = (int16_t)(w * MOTOR_CONST_RIGHT);
		break;
	default:
		HAL_UART_Transmit(&huart3, "w_pwm conv error\n", 17, 100);
		pwm = 0;
	}
	return pwm;
}

void brake() {
	char signal[6];
	signal[0] = 255;
	signal[1] = 255;
	signal[2] = 255;
	signal[3] = 255;
	HAL_UART_Transmit(phuart_arduino, signal, 4, 100);
	return;
}

/*
 * remote.c
 *
 *  Created on: 2018. 6. 19.
 *      Author: leplu
 */

#include "main.h"
#include "u_main.h"
#include "usart.h"

char buffer[3];

void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart){
	if(huart->Instance == huart5.Instance){
		if(buffer[0] == 'W') {
			set_speed(1);
		}
		else if(buffer[0] == 'S') {
			set_speed(-1);
		}
		else if(buffer[0] == 'D') {
			set_rot(1);
		}
		else if(buffer[0] == 'A') {
			set_rot(-1);
		}
		else {
			set_speed(0);
			set_rot(0);
		}
		HAL_UART_Transmit(&huart3, buffer[0], 1, 100);
		HAL_UART_Receive_IT(&huart5, buffer, 1);
	}
	else if(huart->Instance == huart3.Instance){
		HAL_UART_Transmit(&huart5, buffer[0], 1, 100);
		HAL_UART_Receive_IT(&huart3, buffer, 1);
	}
}

void remote_init() {
	HAL_UART_Receive_IT(&huart5, buffer, 1);
	HAL_UART_Receive_IT(&huart3, buffer, 1);
}

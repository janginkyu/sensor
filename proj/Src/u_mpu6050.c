/*
 * u_mpu6050.c
 *
 *  Created on: 2018. 5. 22.
 *      Author: leplu
 */

#include <stdlib.h>

#include "stm32f7xx_hal.h"

#include "u_mpu6050.h"
#include "u_time.h"
#include "u_main.h"

Gyro* u_initialize_gyro(I2C_HandleTypeDef* phi2c_mpu, SD_MPU6050* pmpu_mpu) {
	Gyro* gy = (Gyro*)malloc(sizeof(Gyro));
	gy->gyro = (float*)malloc(sizeof(float) * 3);
	gy->gyro_pre = (float*)malloc(sizeof(float) * 3);
	gy->accel = (float*)malloc(sizeof(float) * 3);
	gy->accel_pre = (float*)malloc(sizeof(float) * 3);
	gy->phi2c = phi2c_mpu;
	gy->pmpu = pmpu_mpu;

	gy->tpre_10us = get10us();
	if (SD_MPU6050_Init(gy->phi2c, gy->pmpu, U_DEVICE, U_ACCEL_SENS, U_GYRO_SENS) == SD_MPU6050_Result_Ok);
	else {
		return NULL;
	}
	SD_MPU6050_ReadGyroscope(gy->phi2c, gy->pmpu);
	int16_t g_x = pmpu_mpu->Gyroscope_X;
	int16_t g_y = pmpu_mpu->Gyroscope_Y;
	int16_t g_z = pmpu_mpu->Gyroscope_Z;
	SD_MPU6050_ReadAccelerometer(gy->phi2c, gy->pmpu);
	int16_t a_x = pmpu_mpu->Accelerometer_X;
	int16_t a_y = pmpu_mpu->Accelerometer_Y;
	int16_t a_z = pmpu_mpu->Accelerometer_Z;
	gy->accel_pre[0] = (float)a_x * pmpu_mpu->Acce_Mult * 9.81;
	gy->accel_pre[1] = (float)a_y * pmpu_mpu->Acce_Mult * 9.81;
	gy->accel_pre[2] = (float)a_z * pmpu_mpu->Acce_Mult * 9.81;
	gy->gyro_pre[0] = (float)g_x * pmpu_mpu->Gyro_Mult * 3.14159265 / 180.;
	gy->gyro_pre[1] = (float)g_y * pmpu_mpu->Gyro_Mult * 3.14159265 / 180.;
	gy->gyro_pre[2] = (float)g_z * pmpu_mpu->Gyro_Mult * 3.14159265 / 180.;

	gy->t_10us = get10us();
	SD_MPU6050_Init(gy->phi2c, gy->pmpu, U_DEVICE, U_ACCEL_SENS, U_GYRO_SENS);
	SD_MPU6050_ReadGyroscope(gy->phi2c, gy->pmpu);
	g_x = pmpu_mpu->Gyroscope_X;
	g_y = pmpu_mpu->Gyroscope_Y;
	g_z = pmpu_mpu->Gyroscope_Z;
	SD_MPU6050_ReadAccelerometer(gy->phi2c, gy->pmpu);
	a_x = pmpu_mpu->Accelerometer_X;
	a_y = pmpu_mpu->Accelerometer_Y;
	a_z = pmpu_mpu->Accelerometer_Z;
	gy->accel[0] = (float)a_x * pmpu_mpu->Acce_Mult * 9.81;
	gy->accel[1] = (float)a_y * pmpu_mpu->Acce_Mult * 9.81;
	gy->accel[2] = (float)a_z * pmpu_mpu->Acce_Mult * 9.81;
	gy->gyro[0] = (float)g_x * pmpu_mpu->Gyro_Mult * 3.14159265 / 180.;
	gy->gyro[1] = (float)g_y * pmpu_mpu->Gyro_Mult * 3.14159265 / 180.;
	gy->gyro[2] = (float)g_z * pmpu_mpu->Gyro_Mult * 3.14159265 / 180.;

	return gy;
}

Gyro* u_update_gyro_value(Gyro* pre) {
	free(pre->accel_pre);
	free(pre->gyro_pre);
	pre->gyro_pre = pre->gyro;
	pre->accel_pre = pre->accel;
	pre->tpre_10us = pre->t_10us;
	pre->accel = (float*)malloc(sizeof(float) * 3);
	pre->gyro = (float*)malloc(sizeof(float) * 3);
	pre->t_10us = get10us();
	while(1) {
		if(SD_MPU6050_Init(pre->phi2c, pre->pmpu, U_DEVICE, U_ACCEL_SENS, U_GYRO_SENS) != SD_MPU6050_Result_Ok) {
#ifdef DEBUG
			HAL_UART_Transmit(&huart3, "!!", 2, 100);
#endif
		}
		else break;
	}
	SD_MPU6050_ReadGyroscope(pre->phi2c, pre->pmpu);
	int16_t g_x = pre->pmpu->Gyroscope_X;
	int16_t g_y = pre->pmpu->Gyroscope_Y;
	int16_t g_z = pre->pmpu->Gyroscope_Z;
	SD_MPU6050_ReadAccelerometer(pre->phi2c, pre->pmpu);
	int16_t a_x = pre->pmpu->Accelerometer_X;
	int16_t a_y = pre->pmpu->Accelerometer_Y;
	int16_t a_z = pre->pmpu->Accelerometer_Z;
	pre->accel[0] = (float)a_x * pre->pmpu->Acce_Mult * 9.81;
	pre->accel[1] = (float)a_y * pre->pmpu->Acce_Mult * 9.81;
	pre->accel[2] = (float)a_z * pre->pmpu->Acce_Mult * 9.81;
	pre->gyro[0] = (float)g_x * pre->pmpu->Gyro_Mult * 3.14159265 / 180.;
	pre->gyro[1] = (float)g_y * pre->pmpu->Gyro_Mult * 3.14159265 / 180.;
	pre->gyro[2] = (float)g_z * pre->pmpu->Gyro_Mult * 3.14159265 / 180.;

	return pre;
}

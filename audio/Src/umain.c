/*
 * umain.c
 *
 *  Created on: 2018. 5. 23.
 *      Author: aaa
 */
#include "umain.h"
#include "tim.h"
#include "dac.h"
#include "main.h"
#include "usart.h"

//sin 함수 round 함수를 위해 필요한 header file
#include "math.h"

#define TWO_PI 3.141592*2

uint8_t hzon[8];

//10us 마다 실행하는 함수
const float hz[8] = {261.6256, 293.6648, 329.6276, 349.2282, 391.9954, 440.0, 493.8833, 523.2511};
uint32_t iter=0;
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim){

	uint8_t i;
	double value = 0.;
	//hz를 주파수로하는 사인 값 계산
	for(i=0; i<8; i++){
		if(hzon[i]) {
			value = sin(hz[i]*TWO_PI*iter*10.0f/1000.0f/1000.0f);
			break;
		}
	}
	//value -1~1 의 값을 1024~3072 로 변환
	//1024 값을 조절하면 진폭이 변경됨
	uint32_t dacValue=round(value*1024.0f+2048.0f);
	HAL_DAC_SetValue(&hdac, DAC1_CHANNEL_2, DAC_ALIGN_12B_R, dacValue);
	iter++;
}
/*
void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin) {
	switch(GPIO_Pin) {
	case SW1_Pin:
		hzon[0] = !hzon[0];
		break;
	case SW2_Pin:
		hzon[1] = !hzon[1];
		break;
	case SW3_Pin:
		hzon[2] = !hzon[2];
		break;
	case SW4_Pin:
		hzon[3] = !hzon[3];
		break;
	case SW5_Pin:
		hzon[4] = !hzon[4];
		break;
	case SW6_Pin:
		hzon[5] = !hzon[5];
		break;
	case SW7_Pin:
		hzon[6] = !hzon[6];
		break;
	case SW8_Pin:
		hzon[7] = !hzon[7];
		break;
	default:
		break;
	}
}
*/
void uInit(){
	uint8_t i;
	//Dac Channel 2 Start
	HAL_DAC_Start(&hdac, DAC_CHANNEL_2);
	//Timer 1 Start;
	for(i=0; i<8; i++) {
		hzon[i] = 0;
	}
	HAL_TIM_Base_Start_IT(&htim1);
	HAL_UART_Transmit(&huart3, "!", 1, 100);
}

void uLoop(){
	hzon[0] = (HAL_GPIO_ReadPin(SW1_GPIO_Port, SW1_Pin))?1:0;
	hzon[1] = (HAL_GPIO_ReadPin(SW2_GPIO_Port, SW2_Pin))?1:0;
	hzon[2] = (HAL_GPIO_ReadPin(SW3_GPIO_Port, SW3_Pin))?1:0;
	hzon[3] = (HAL_GPIO_ReadPin(SW4_GPIO_Port, SW4_Pin))?1:0;
	hzon[4] = (HAL_GPIO_ReadPin(SW5_GPIO_Port, SW5_Pin))?1:0;
	hzon[5] = (HAL_GPIO_ReadPin(SW6_GPIO_Port, SW6_Pin))?1:0;
	hzon[6] = (HAL_GPIO_ReadPin(SW7_GPIO_Port, SW7_Pin))?1:0;
	hzon[7] = (HAL_GPIO_ReadPin(SW8_GPIO_Port, SW8_Pin))?1:0;

}
